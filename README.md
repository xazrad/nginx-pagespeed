Настройки pagespeed добавлены в контектс http {} файла nginx.conf и действуют для всех сайтов.


Файл настроек `pagespeed.conf` импортируется `include /etc/nginx/conf.d/*.conf;` в файле nginx.conf.


Файл кеша преобразованных файлов `/var/cache/ngx_pagespeed/`.


В Response Headers должно появиться x-page-speed:*.

Для облегчения настройки PageSpeed имеет три уровня конфигурации.

CoreFilters - максимальный набор фильтров подходящий для работы большинства сайтов. Является уровнем по умолчанию и активируется при запуске PageSpeed без дополнительных настроек.

OptimizeForBandwidth - минимальный набор фильтров. В основном оптимизирует и сжимает код.

PassThrough - полностью отключает все фильтры.

Уровни определяются директивой pagespeed RewriteLevel.

pagespeed RewriteLevel CoreFilters;


```
 docker build -t custom-nginx . 
 ```


```
docker run --rm -p80:80 custom-nginx:latest
```

### Base Image

https://github.com/We-Amp/ngx-pagespeed-alpine


### Other

https://techlist.top/%D0%BD%D0%B0%D1%81%D1%82%D1%80%D0%BE%D0%B9%D0%BA%D0%B0-pagespeed-%D0%B2-nginx/

